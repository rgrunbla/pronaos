# Organisation du dépôt

Le dépôt est organisé en plusieurs sous-dossiers:

- [Infrastructure](Infrastructure) contient les playbooks ansible nécessaire à la gestion des serveurs
- [Secrets](Secrets) contient les secrets (mots de passe, …) liés à Pronaos

## Développement

Le gestionnaire de paquet [nix](https://nixos.org/) est utilisé pour l'environnement de développement de ce projet. Merci de commencer par un `nix develop` pour installer les dépendances nécessaires au bon fonctionnement de ce dépôt.
