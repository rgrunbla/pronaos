# Utilisation

1. Cloner le dépôt ;
2. Installer `sops` et `age` (ou, en utilisant `nix`, lancer `nix develop`) ;
3. Se générer une clef age avec `age-keygen -o ~/.age/age_private_key` (créer le dossier `~/.age/` si besoin) ;
3. Ajouter sa clef publique `age` dans le fichier [.sops.yaml](./.sops.yaml) avec un username, e.g. pour `nouvelutilisateur` :

```
keys:
  - &remy age1ry4cavlttla9q2a8sq9na8plp5vpjpyqfsckg8z9y0gg04swzd2qa7tpnp
  - &nouvelutilisateur age1ql3z7hjy54pw3hyww5ayyfg7zqgvc7w3j2elw8zmrj2kg5sfn9aqmcac8p
creation_rules:
  - path_regex: secrets/*
    key_groups:
      - age:
        - *remy
        - *nouvelutilisateur
```

4. Demander à une personne possédant un accès au dépôt de rechiffrer les secrets avec la commande suivante:

```
SOPS_AGE_KEY_FILE=~/.age/age_private_key sops updatekeys -y secrets/*
```

5. Pour déchiffrer les secrets, utiliser la commande suivante :
```
SOPS_AGE_KEY_FILE=~/.age/age_private_key sops secrets/test
```
6. Pour ajouter un secret, utiliser la commande suivante :
```
SOPS_AGE_KEY_FILE=~/.age/age_private_key sops secrets/mon_nouveau_secret.yaml
```

Note: dans le shell `nix develop`, la variable d'environnement `SOPS_AGE_KEY_FILE` contient `~/.age/age_private_key`.
On peut donc directement utiliser `sops secrets/test`, par exemple.
