{
  description = "Development Environment for Pronaos";
  inputs = { nixpkgs.url = "nixpkgs/nixos-22.11"; };

  outputs = { self, nixpkgs }: with import nixpkgs { system = "x86_64-linux"; overlays = [ ]; }; {
    devShell.x86_64-linux = with import nixpkgs { system = "x86_64-linux"; };
      mkShell {
        buildInputs = [
          ansible
          age
          ssh-to-age
          dhall-bash
          dhall-json
        ];
        shellHook = ''
            export SOPS_AGE_KEY_FILE=~/.age/age_private_key
            ansible-galaxy collection install community.sops community.general
        '';
      };
  };
}
