#!/usr/bin/env bash

echo '=====> Updating apt…'
sudo apt -y update
echo '=====> Installing dependencies…'
sudo apt -y install mkosi qemu qemu-system-x86 python3 kpartx virtualbox wget
wget "https://github.com/dhall-lang/dhall-haskell/releases/download/1.41.2/dhall-1.41.2-x86_64-Linux.tar.bz2"
tar --extract --file dhall-1.41.2-x86_64-Linux.tar.bz2
echo '=====> Using dhall to generate the configs'
echo '(./mkosi.dhall).postconfScript' | ./bin/dhall text > postconf.sh
echo '======> Generated postconf.sh is :'
cat postconf.sh
echo '(./mkosi.dhall).postconfFstab' | ./bin/dhall text > fstab
echo '======> Generated fstab is :'
cat fstab
echo '(./mkosi.dhall).postinst' | ./bin/dhall text > mkosi.postinst
chmod +x mkosi.postinst
echo '======> Generated mkosi.postinst is :'
cat mkosi.postinst
echo '(./mkosi.dhall).postconf' | ./bin/dhall text > post_conf.py
chmod +x post_conf.py
echo '======> Generated post_conf.py is :'
cat post_conf.py
echo '=====> Building the live…'
sudo mkosi --workspace-dir=/tmp/
echo '=====> Booting the live in background…'
qemu-system-x86_64 \
    -bios UEFI/OVMF.fd \
    -enable-kvm \
    -cpu host \
    -m 4G \
    -drive file=pronaos.img,index=0,media=disk \
    -boot c \
    -net user \
    -nic user \
    -net nic \
    -msg timestamp=on \
    -chardev socket,path=/tmp/qga.sock,server=on,wait=off,id=qga0 \
    -device virtio-serial \
    -nographic \
    -device virtserialport,chardev=qga0,name=org.qemu.guest_agent.0 &
echo '=====> Waiting for the live to be booted…'
sleep 60
echo '=====> Running the post script'
./post_conf.py
echo '=====> Waiting for shutdown of the live'
sleep 120
echo '=====> Shrink down the image, but leave a bit of room (1G) by default'
./resize.sh
truncate -s +2G pronaos.img
echo '=====> Create the VDI image'
mkdir -p /result/pronaos/
export OVF_ID=$(echo '(./pronaos.dhall).virtualboxInfo.ovfId' | ./bin/dhall text)
VBoxManage convertdd pronaos.img /result/pronaos/${OVF_ID}-disk001.vmdk --format VMDK
echo '(./virtualbox.dhall).virtualboxOvf' | ./bin/dhall text > /result/pronaos/${OVF_ID}.ovf
export OVF_SHA1_SUM=$(sha1sum /result/pronaos/${OVF_ID}.ovf | head -c 40)
export VMDK_SHA1_SUM=$(sha1sum /result/pronaos/${OVF_ID}-disk001.vmdk | head -c 40)
printf "SHA1 (${OVF_ID}.ovf) = ${OVF_SHA1_SUM}\nSHA1 (${OVF_ID}-disk001.vmdk) = ${VMDK_SHA1_SUM}" > /result/pronaos/${OVF_ID}.mf
cd /result/pronaos/ && tar -cvf ../pronaos.ova * && cd -
rm -rf /result/pronaos/
echo '=====> Compress the RAW image'
zstd pronaos.img -o /result/pronaos.img.zst
sudo chmod a+r /result/pronaos.img.zst
