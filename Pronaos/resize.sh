#!/usr/bin/env bash

echo "Syncing changes…"
sudo partprobe
sudo sync

export LANG=C
export IMAGE="./pronaos.img"
export KPARTX=$(sudo kpartx -avs ${IMAGE})

echo "kpartx -avs ${IMAGE} output is ${KPARTX}"

while read first_line; read second_line;
do
    export PART_BOOT="${first_line}";
    export PART_ROOT="${second_line}";
    export LOOP_DEV=$(echo ${first_line}|grep -o 'loop[0-9]\+')
done <<<$(grep -o 'loop[0-9]*p[0-9]*' <<<"$KPARTX")

echo "Loop Device is /dev/${LOOP_DEV}"
echo "Boot partition is /dev/mapper/${PART_BOOT}"
echo "Root partition is /dev/mapper/${PART_ROOT}"

echo "Running 'sudo sgdisk -p /dev/${LOOP_DEV}'"
sudo sgdisk -p /dev/${LOOP_DEV}

echo "Running 'sudo e2fsck -p -f /dev/mapper/${PART_ROOT}'"
sudo e2fsck -p -f /dev/mapper/${PART_ROOT}

echo "Syncing changes…"
sudo partprobe
sudo partprobe /dev/${LOOP_DEV}
sudo sync

echo "Running 'sudo resize2fs -M /dev/mapper/${PART_ROOT}'"
sudo resize2fs -M /dev/mapper/${PART_ROOT}

echo "Syncing changes…"
sudo partprobe
sudo partprobe /dev/${LOOP_DEV}
sudo sync

echo "Running 'sudo sgdisk -p /dev/${LOOP_DEV}'"
sudo sgdisk -p /dev/${LOOP_DEV}

echo "Running 'sudo tune2fs -l /dev/mapper/${PART_ROOT}'"
sudo tune2fs -l /dev/mapper/${PART_ROOT}

echo "Syncing changes…"
sudo partprobe
sudo partprobe /dev/${LOOP_DEV}
sudo sync

export BLOCK_COUNT=$(sudo tune2fs -l /dev/mapper/${PART_ROOT} | grep "Block count" | grep -o "[0-9]\+")
export BLOCK_SIZE=$(sudo tune2fs -l /dev/mapper/${PART_ROOT} | grep "Block size" | grep -o "[0-9]\+")
export SECTOR_SIZE=$(sudo fdisk -l /dev/${LOOP_DEV}|grep "Sector size"| sed "s/.* \([0-9]\+\) .*/\1/g")
export SECTOR_START=$(sudo fdisk -l /dev/${LOOP_DEV}|grep "${PART_ROOT}"|awk '{print $2}')
export SECTOR_END=$(python3 -c "print(int(10+${SECTOR_START}+(${BLOCK_COUNT}*${BLOCK_SIZE})/${SECTOR_SIZE}))")
export TRUNCATE_SIZE=$(python3 -c "print(int((${SECTOR_END}+40)*${SECTOR_SIZE}))")

echo "Block Count is ${BLOCK_COUNT}"
echo "Block Size is ${BLOCK_SIZE}"
echo "Sector Size is ${SECTOR_SIZE}"
echo "Sector Start is ${SECTOR_START}"
echo "New End Sector is ${SECTOR_END}"
echo "Truncate Size is ${TRUNCATE_SIZE}"

echo "Syncing changes…"
sudo partprobe
sudo partprobe /dev/${LOOP_DEV}
sudo sync

echo "Running 'sudo kpartx -dv /dev/${LOOP_DEV}'"
sudo kpartx -dv /dev/${LOOP_DEV}

echo "Syncing changes…"
sudo partprobe
sudo partprobe /dev/${LOOP_DEV}
sudo sync

echo "Running 'sudo sgdisk -p ${IMAGE}'"
sudo sgdisk -p ${IMAGE}

echo "Running 'sudo sgdisk -d 2 -n 2:${SECTOR_START}:${SECTOR_END} -t 2:8304 -c 0:\"root\" ${IMAGE}'"
sudo sgdisk -d 2 -n 2:${SECTOR_START}:${SECTOR_END} -t 2:8304 -c 0:"root" ${IMAGE}

echo "Syncing changes…"
sudo partprobe
sudo sync

echo "Running 'sudo sgdisk -p ${IMAGE}'"
sudo sgdisk -p ${IMAGE}

echo "Truncating image…"
echo "Running 'sudo truncate --size=${TRUNCATE_SIZE} ${IMAGE}"
sudo truncate --size=${TRUNCATE_SIZE} ${IMAGE}
printf 'w\ny\n' | gdisk ${IMAGE}

echo "Syncing changes…"
sudo partprobe
sudo sync
