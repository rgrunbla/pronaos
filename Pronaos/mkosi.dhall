let Prelude =
      https://prelude.dhall-lang.org/v19.0.0/package.dhall
        sha256:eb693342eb769f782174157eba9b5924cf8ac6793897fc36a31ccbd6f56dafe2

let quoteList =
      \(packages : List Text) ->
        Prelude.List.map Text Text (\(val : Text) -> "\"${val}\"") packages

let printList =
      \(packages : List Text) -> Prelude.Text.concatSep " " (quoteList packages)

let formatLocales =
      \(locales : List Text) -> Prelude.Text.concatSep ", " locales

let pronaosConfig = ./pronaos.dhall

let postinst =
      ''
      #!/usr/bin/env bash

      export DEBIAN_FRONTEND=noninteractive

      apt-get -y update
      apt-get install -y tmux
      apt-get install -y debconf-utils

      cat <<EOF > /etc/selections.debconf
      locales	locales/default_environment_locale	select	${pronaosConfig.i18n.defaultLocale}
      locales	locales/locales_to_be_generated	multiselect	${formatLocales
                                                              pronaosConfig.i18n.generatedLocales}
      keyboard-configuration	keyboard-configuration/layout	select	${pronaosConfig.i18n.keyboard.layout}
      keyboard-configuration	keyboard-configuration/layoutcode	string	${pronaosConfig.i18n.keyboard.layoutcode}
      keyboard-configuration	keyboard-configuration/modelcode	string	${pronaosConfig.i18n.keyboard.modelcode}
      keyboard-configuration	keyboard-configuration/variant	select	${pronaosConfig.i18n.keyboard.variant}
      console-setup	console-setup/codeset47	select	Guess optimal character set
      ubiquity	localechooser/languagelist	select	${pronaosConfig.i18n.language}
      ubiquity	mirror/http/countries	select	${pronaosConfig.i18n.mirrorCountry}
      EOF

      debconf-set-selections < /etc/selections.debconf

      apt-get -y upgrade
      apt-get install -y ${printList pronaosConfig.basePackages}

      # purge
      apt-get purge -y ${printList pronaosConfig.purgePackages}

      # Language Support
      # FIXME: why is this not done automatically ?
      echo "Checking Language Support"
      echo `check-language-support -l ${pronaosConfig.i18n.language}`
      sudo apt-get -y install `check-language-support -l ${pronaosConfig.i18n.language}`


      cat <<EOF > /etc/systemd/system/growpart-and-resizefs-root.service
      [Unit]
      Description=Extend root partition and resize ext4 file system
      After=local-fs.target
      Wants=local-fs.target

      [Service]
      Environment=ROOT_DISK=/dev/sda
      Environment=ROOT_PARTITION=2
      ExecStart=/bin/bash -c "/usr/bin/growpart -N \''${ROOT_DISK} \''${ROOT_PARTITION} && /usr/bin/growpart \''${ROOT_DISK} \''${ROOT_PARTITION} || exit 0"
      ExecStop=/bin/bash -c "/sbin/resize2fs \''${ROOT_DISK}\''${ROOT_PARTITION} || exit 0"
      Type=oneshot

      [Install]
      WantedBy=multi-user.target
      EOF

      ln -s /etc/systemd/system/growpart-and-resizefs-root.service /etc/systemd/system/multi-user.target.wants/growpart-and-resizefs-root.service


      # remove unused and clean up apt cache
      #apt-get autoremove -y

      # final touch
      dpkg-reconfigure keyboard-configuration
      dpkg-reconfigure console-data
      dpkg-reconfigure locales
      dpkg-reconfigure resolvconf

      # network manager
      cat <<EOF > /etc/NetworkManager/NetworkManager.conf
      [main]
      rc-manager=resolvconf
      plugins=ifupdown,keyfile
      dns=dnsmasq

      [ifupdown]
      managed=false
      EOF

      dpkg-reconfigure network-manager

      apt-get clean -y

      groupadd -r autologin
      useradd -m ${pronaosConfig.user.username} -s /bin/bash -G sudo,autologin
      echo -e "${pronaosConfig.user.password}\n${pronaosConfig.user.password}"|passwd ${pronaosConfig.user.username}

      mkdir -p /etc/lightdm
      cat <<EOF > /etc/lightdm/lightdm.conf 
      [Seat:*]
      autologin-guest=false
      autologin-user=${pronaosConfig.user.username}
      autologin-user-timeout=0
      EOF

      mkdir -p /usr/share/man/man1/
      ''

let postconf =
      ''
      #!/usr/bin/env python3
      import socket
      import time
      import json
      import base64
      import re
      from io import BytesIO

      def recv(sock):
          BUFF_SIZE = 4096
          data = b'''
          while True:
              part = sock.recv(BUFF_SIZE)
              data += part
              if data.endswith(b"\n"):
                  break
          return data

      def execute_human(sock, command_line, stop_on_error=True):
          env=["DEBIAN_FRONTEND=noninteractive"]
          splitted = re.split('''' (?=(?:[^'"]|'[^']*'|"[^"]*")*$)'''', command_line)
          data = execute(sock, splitted[0], arguments=splitted[1:], env=env, stop_on_error=stop_on_error)
          try:
              print(data, flush=True)
          except BlockingIOError:
              print("There has been an `BlockingIOError` error but that should not be this much of a problem.")
          return data

      def execute(sock, command, arguments=[], env=[], stop_on_error=True):
          command_payload = {
                  'execute': 'guest-exec',
                  'arguments': {
                      "path": command,
                      "arg": arguments,
                      "capture-output": True,
                      "env" : env,
                      }
                  }
          encoded_command_payload = json.dumps(command_payload).encode('utf-8')
          sock.send(encoded_command_payload)
          data = recv(sock)
          d = json.loads(data.decode('utf-8'))
          pid = d['return']['pid']
          print(f"PID is {pid}")

          result_payload = {
                  'execute': "guest-exec-status",
                  "arguments": {
                      "pid": pid
                      }
                  }
          encoded_result_payload = json.dumps(result_payload).encode('utf-8')
          exited = False
          exitcode = None
          while True:
              sock.send(encoded_result_payload)
              data = recv(sock)
              result = json.loads(data)
              if result['return']['exited']:
                  exitcode = result['return']['exitcode']
                  break
              time.sleep(1)

          if stop_on_error and exitcode != 0:
              print(result)
              print(f"Exit code was {exitcode}, aborting.")
              exit(-1)

          if 'out-data' in result['return']:
              base64_output = result['return']['out-data']
              return base64.b64decode(base64_output).decode()
          elif 'err-data' in result['return']:
              base64_output = result['return']['err-data']
              return base64.b64decode(base64_output).decode()
          return None

      def wait_for(sock, pattern, delay=60):
          while True:
              retval = execute_human(sock, f"pgrep -f '{pattern}'", stop_on_error=False)
              if not retval:
                  break
              print(f"Looping and waiting for the absence of `{pattern}` in the running processes.", flush=True)
              time.sleep(delay)

      def write_file(sock, content, path, mode="w+"):
          command_payload = {
                  'execute': 'guest-file-open',
                  'arguments': {
                      "path": path,
                      "mode": mode
                      }
                  }
          encoded_command_payload = json.dumps(command_payload).encode('utf-8')
          sock.send(encoded_command_payload)
          data = recv(sock)
          d = json.loads(data.decode('utf-8'))
          handle = d['return']
          print(f"Handle is {handle}", flush=True)
          encoded_content = base64.b64encode(content.encode('utf-8')).decode('utf-8')
          result_payload = {
                  'execute': "guest-file-write",
                  "arguments": {
                      "handle": handle,
                      "buf-b64": encoded_content
                      }
                  }
          encoded_result_payload = json.dumps(result_payload).encode('utf-8')
          sock.send(encoded_result_payload)
          data = recv(sock)
          result = json.loads(data)
          print(result)
          result_payload = {
                  'execute': "guest-file-close",
                  "arguments": {
                      "handle": handle,
                      }
                  }
          encoded_result_payload = json.dumps(result_payload).encode('utf-8')
          sock.send(encoded_result_payload)
          data = recv(sock)
          result = json.loads(data)
          print(result)

      sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
      sock.connect("/tmp/qga.sock")

      print("Writing the `fstab` file.")
      with open('fstab', 'r') as fstab:
          fstab_str = fstab.read()
          data = write_file(sock, fstab_str, "/etc/fstab")
          print(data)

      print("Writing the `postconf.sh` file.", flush=True)
      with open('postconf.sh', 'r') as script:
          script_str = script.read()
          data = write_file(sock, script_str, "/tmp/postconf.sh")
          print(data)

      print("Making the `postconf.sh` file executable.", flush=True)
      execute_human(sock, "chmod +x /tmp/postconf.sh")

      print("Running the `postconf.sh` file.", flush=True)
      execute_human(sock, "bash -c /tmp/postconf.sh", stop_on_error=False)
      wait_for(sock, "postconf.sh")

      print("Sync and stop the VM")
      execute_human(sock, "sync")
      execute_human(sock, "shutdown -h now")
      sock.close()
      ''

let postconfFstab =
      ''
      LABEL=root	/	ext4	rw,relatime	0	1
      LABEL=EFI	/efi	vfat	rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro	0	2
      ''

let postconfScript =
      ''
      #!/usr/bin/env bash

      export DEBIAN_FRONTEND=noninteractive

      # Take care of the secure boot
      apt-get -qq install -y grub-efi-amd64-signed shim-signed
      bootctl remove --no-pager || true
      update-initramfs -c -k $(uname -r)
      grub-install
      update-grub

      # APT packages
      apt-get install -y ${printList pronaosConfig.pronaosPackages}

      # Snap packages
      for package in ${printList pronaosConfig.snapPackages}
        do snap install $package
      done

      # Snap classic packages
      for package in ${printList pronaosConfig.snapPackagesClassic}
        do snap install $package --classic
      done

      # PIP3 packages
      for package in ${printList pronaosConfig.pythonPackages}
        do python3 -m pip install "$package"
      done

      # Enable phpmyadmin
      ln -s /usr/share/phpmyadmin/ /var/www/html
      mysql -e "create user ${pronaosConfig.user.mysql_username} identified by '${pronaosConfig.user.mysql_password}'"
      mysql -e "grant all privileges on *.* to ${pronaosConfig.user.username} with grant option"
      mysql -e "flush privileges"

      # Opam packages
      su ${pronaosConfig.user.username} -c "opam init -a"
      su ${pronaosConfig.user.username} -c "opam install -y ${printList
                                                                pronaosConfig.opamPackages}"
      su ${pronaosConfig.user.username} -c "eval \$(opam env) && ocaml-jupyter-opam-genspec"
      su ${pronaosConfig.user.username} -c "echo 'eval \$(opam env)' >> /home/${pronaosConfig.user.username}/.bashrc"
      su ${pronaosConfig.user.username} -c "jupyter kernelspec install --user --name ocaml-jupyter /home/${pronaosConfig.user.username}/.opam/default/share/jupyter"
      su ${pronaosConfig.user.username} -c "opam clean"

      # Various configuration
      su ${pronaosConfig.user.username} -c "mkdir -p /home/${pronaosConfig.user.username}/.config/VSCodium"
      su ${pronaosConfig.user.username} -c "mkdir -p /home/${pronaosConfig.user.username}/.vscode-oss/extensions"

      # Codium packages
      su ${pronaosConfig.user.username} -c "wget https://github.com/microsoft/vscode-cpptools/releases/download/v1.14.3/cpptools-linux.vsix"
      for extension in ${printList pronaosConfig.codiumPackages}
        do su ${pronaosConfig.user.username} -c "codium --install-extension $extension"
      done

      # Bit of cleaning
      apt-get clean
      rm -rf /var/lib/snapd/cache/*
      apt-get autoremove

      # Set hostname
      hostnamectl set-hostname ${pronaosConfig.hostname}
      sed -i "s/localhost/${pronaosConfig.hostname}/g" /etc/hosts
      ''

in  { postinst, postconf, postconfScript, postconfFstab }
