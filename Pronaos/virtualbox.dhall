let Prelude =
      https://prelude.dhall-lang.org/v19.0.0/package.dhall
        sha256:eb693342eb769f782174157eba9b5924cf8ac6793897fc36a31ccbd6f56dafe2

let pronaosConfig = ./pronaos.dhall

let virtualboxOvf =
      ''
      <?xml version="1.0"?>
      <Envelope ovf:version="1.0" xml:lang="en-US" xmlns="http://schemas.dmtf.org/ovf/envelope/1" xmlns:ovf="http://schemas.dmtf.org/ovf/envelope/1" xmlns:rasd="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_ResourceAllocationSettingData" xmlns:vssd="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_VirtualSystemSettingData" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:vbox="http://www.virtualbox.org/ovf/machine">
        <References>
          <File ovf:id="file1" ovf:href="${pronaosConfig.virtualboxInfo.ovfId}-disk001.vmdk"/>
        </References>
        <DiskSection>
          <Info>List of the virtual disks used in the package</Info>
          <Disk ovf:capacity="16683885568" ovf:diskId="vmdisk1" ovf:fileRef="file1" ovf:format="http://www.vmware.com/interfaces/specifications/vmdk.html#streamOptimized" vbox:uuid="d204da67-f626-4e90-8f96-2aa90bde0481"/>
        </DiskSection>
        <NetworkSection>
          <Info>Logical networks used in the package</Info>
          <Network ovf:name="NAT">
            <Description>Logical network used by this appliance.</Description>
          </Network>
        </NetworkSection>
        <VirtualSystem ovf:id="${pronaosConfig.virtualboxInfo.ovfId}">
          <Info>A virtual machine</Info>
          <ProductSection>
            <Info>Meta-information about the installed software</Info>
            <ProductUrl>${pronaosConfig.virtualboxInfo.productUrl}</ProductUrl>
          </ProductSection>
          <AnnotationSection>
            <Info>A human-readable annotation</Info>
            <Annotation>${pronaosConfig.virtualboxInfo.productAnnotation}</Annotation>
          </AnnotationSection>
          <OperatingSystemSection ovf:id="94">
            <Info>The kind of installed guest operating system</Info>
            <Description>Ubuntu_64</Description>
            <vbox:OSType ovf:required="false">Ubuntu_64</vbox:OSType>
          </OperatingSystemSection>
          <VirtualHardwareSection>
            <Info>Virtual hardware requirements for a virtual machine</Info>
            <System>
              <vssd:ElementName>Virtual Hardware Family</vssd:ElementName>
              <vssd:InstanceID>0</vssd:InstanceID>
              <vssd:VirtualSystemIdentifier>${pronaosConfig.virtualboxInfo.ovfId}</vssd:VirtualSystemIdentifier>
              <vssd:VirtualSystemType>virtualbox-2.2</vssd:VirtualSystemType>
            </System>
            <Item>
              <rasd:Caption>${pronaosConfig.virtualboxInfo.cpuCount} virtual CPU</rasd:Caption>
              <rasd:Description>Number of virtual CPUs</rasd:Description>
              <rasd:ElementName>${pronaosConfig.virtualboxInfo.cpuCount} virtual CPU</rasd:ElementName>
              <rasd:InstanceID>1</rasd:InstanceID>
              <rasd:ResourceType>3</rasd:ResourceType>
              <rasd:VirtualQuantity>${pronaosConfig.virtualboxInfo.cpuCount}</rasd:VirtualQuantity>
            </Item>
            <Item>
              <rasd:AllocationUnits>MegaBytes</rasd:AllocationUnits>
              <rasd:Caption>${pronaosConfig.virtualboxInfo.ramSize} MB of memory</rasd:Caption>
              <rasd:Description>Memory Size</rasd:Description>
              <rasd:ElementName>${pronaosConfig.virtualboxInfo.ramSize} MB of memory</rasd:ElementName>
              <rasd:InstanceID>2</rasd:InstanceID>
              <rasd:ResourceType>4</rasd:ResourceType>
              <rasd:VirtualQuantity>${pronaosConfig.virtualboxInfo.ramSize}</rasd:VirtualQuantity>
            </Item>
            <Item>
              <rasd:Address>0</rasd:Address>
              <rasd:Caption>ideController0</rasd:Caption>
              <rasd:Description>IDE Controller</rasd:Description>
              <rasd:ElementName>ideController0</rasd:ElementName>
              <rasd:InstanceID>3</rasd:InstanceID>
              <rasd:ResourceSubType>PIIX4</rasd:ResourceSubType>
              <rasd:ResourceType>5</rasd:ResourceType>
            </Item>
            <Item>
              <rasd:Address>1</rasd:Address>
              <rasd:Caption>ideController1</rasd:Caption>
              <rasd:Description>IDE Controller</rasd:Description>
              <rasd:ElementName>ideController1</rasd:ElementName>
              <rasd:InstanceID>4</rasd:InstanceID>
              <rasd:ResourceSubType>PIIX4</rasd:ResourceSubType>
              <rasd:ResourceType>5</rasd:ResourceType>
            </Item>
            <Item>
              <rasd:Address>0</rasd:Address>
              <rasd:Caption>sataController0</rasd:Caption>
              <rasd:Description>SATA Controller</rasd:Description>
              <rasd:ElementName>sataController0</rasd:ElementName>
              <rasd:InstanceID>5</rasd:InstanceID>
              <rasd:ResourceSubType>AHCI</rasd:ResourceSubType>
              <rasd:ResourceType>20</rasd:ResourceType>
            </Item>
            <Item>
              <rasd:Address>0</rasd:Address>
              <rasd:Caption>usb</rasd:Caption>
              <rasd:Description>USB Controller</rasd:Description>
              <rasd:ElementName>usb</rasd:ElementName>
              <rasd:InstanceID>6</rasd:InstanceID>
              <rasd:ResourceType>23</rasd:ResourceType>
            </Item>
            <Item>
              <rasd:AddressOnParent>3</rasd:AddressOnParent>
              <rasd:AutomaticAllocation>false</rasd:AutomaticAllocation>
              <rasd:Caption>sound</rasd:Caption>
              <rasd:Description>Sound Card</rasd:Description>
              <rasd:ElementName>sound</rasd:ElementName>
              <rasd:InstanceID>7</rasd:InstanceID>
              <rasd:ResourceSubType>ensoniq1371</rasd:ResourceSubType>
              <rasd:ResourceType>35</rasd:ResourceType>
            </Item>
            <Item>
              <rasd:AddressOnParent>0</rasd:AddressOnParent>
              <rasd:Caption>disk1</rasd:Caption>
              <rasd:Description>Disk Image</rasd:Description>
              <rasd:ElementName>disk1</rasd:ElementName>
              <rasd:HostResource>/disk/vmdisk1</rasd:HostResource>
              <rasd:InstanceID>8</rasd:InstanceID>
              <rasd:Parent>5</rasd:Parent>
              <rasd:ResourceType>17</rasd:ResourceType>
            </Item>
            <Item>
              <rasd:AutomaticAllocation>true</rasd:AutomaticAllocation>
              <rasd:Caption>Ethernet adapter on 'NAT'</rasd:Caption>
              <rasd:Connection>NAT</rasd:Connection>
              <rasd:ElementName>Ethernet adapter on 'NAT'</rasd:ElementName>
              <rasd:InstanceID>9</rasd:InstanceID>
              <rasd:ResourceSubType>E1000</rasd:ResourceSubType>
              <rasd:ResourceType>10</rasd:ResourceType>
            </Item>
          </VirtualHardwareSection>
          <vbox:Machine ovf:required="false" version="1.16-linux" uuid="{60f6151d-9757-42b3-ad0c-a0b91fa629ae}" name="Pronaos" OSType="Ubuntu_64" snapshotFolder="Snapshots" lastStateChange="2023-02-26T10:06:12Z">
            <ovf:Info>Complete VirtualBox machine configuration in VirtualBox format</ovf:Info>
            <ExtraData>
              <ExtraDataItem name="GUI/LastNormalWindowPosition" value="640,279,640,522,max"/>
            </ExtraData>
            <Hardware>
              <CPU count="${pronaosConfig.virtualboxInfo.cpuCount}">
                <PAE enabled="false"/>
                <LongMode enabled="true"/>
                <X2APIC enabled="true"/>
                <HardwareVirtExLargePages enabled="false"/>
              </CPU>
              <Memory RAMSize="${pronaosConfig.virtualboxInfo.ramSize}"/>
              <Firmware type="EFI"/>
              <HID Pointing="USBTablet"/>
              <Display controller="VMSVGA" VRAMSize="16"/>
              <BIOS>
                <IOAPIC enabled="true"/>
                <SmbiosUuidLittleEndian enabled="true"/>
              </BIOS>
              <USB>
                <Controllers>
                  <Controller name="OHCI" type="OHCI"/>
                </Controllers>
              </USB>
              <Network>
                <Adapter slot="0" enabled="true" type="82540EM">
                  <NAT/>
                </Adapter>
              </Network>
              <AudioAdapter codec="AD1980" driver="ALSA" enabled="true" enabledIn="false"/>
              <RTC localOrUTC="UTC"/>
              <Clipboard/>
              <GuestProperties>
                <GuestProperty name="/VirtualBox/HostInfo/GUI/LanguageID" value="C" timestamp="1677405972742608000" flags=""/>
              </GuestProperties>
            </Hardware>
            <StorageControllers>
              <StorageController name="IDE" type="PIIX4" PortCount="2" useHostIOCache="true" Bootable="true">
                <AttachedDevice passthrough="false" type="DVD" hotpluggable="false" port="1" device="0"/>
              </StorageController>
              <StorageController name="SATA" type="AHCI" PortCount="1" useHostIOCache="false" Bootable="true" IDE0MasterEmulationPort="0" IDE0SlaveEmulationPort="1" IDE1MasterEmulationPort="2" IDE1SlaveEmulationPort="3">
                <AttachedDevice type="HardDisk" hotpluggable="false" port="0" device="0">
                  <Image uuid="{d204da67-f626-4e90-8f96-2aa90bde0481}"/>
                </AttachedDevice>
              </StorageController>
            </StorageControllers>
          </vbox:Machine>
        </VirtualSystem>
      </Envelope>
      ''

in  { virtualboxOvf }
