-- packages.dhall
let basePackages
    : List Text
    = [ "apt-transport-https"
      , "casper"
      , "clamav-daemon"
      , "cloud-guest-utils"
      , "curl"
      , "discover"
      , "laptop-detect"
      , "less"
      , "locales"
      , "nano"
      , "net-tools"
      , "network-manager"
      , "os-prober"
      , "plymouth-theme-ubuntu-logo"
      , "qemu-guest-agent"
      , "resolvconf"
      , "snapd"
      , "sudo"
      , "terminator"
      , "ubiquity"
      , "ubiquity-frontend-gtk"
      , "ubiquity-slideshow-ubuntu"
      , "ubiquity-ubuntu-artwork"
      , "ubuntu-gnome-desktop"
      , "ubuntu-gnome-wallpapers"
      , "ubuntu-standard"
      , "vim"
      , "wireless-tools"
      , "wpagui"
      , "xubuntu-desktop"
      ]

let purgePackages
    : List Text
    = [ "transmission-gtk"
      , "transmission-common"
      , "gnome-mahjongg"
      , "gnome-mines"
      , "gnome-sudoku"
      , "aisleriot"
      ]

let pronaosPackages
    : List Text
    = [ "apache2"
      , "bash-completion"
      , "build-essential"
      , "chromium-browser"
      , "emacs"
      , "evince"
      , "flex"
      , "gcc-10"
      , "gdb"
      , "gfortran"
      , "gimp"
      , "git"
      , "glibc-doc"
      , "gnome-terminal"
      , "gnuplot"
      , "gprolog"
      , "graphviz"
      , "htop"
      , "inkscape"
      , "language-pack-fr"
      , "libffi-dev"
      , "libgmp-dev"
      , "libreoffice"
      , "libreoffice-l10n-fr"
      , "libzmq5-dev"
      , "man-db"
      , "manpages"
      , "manpages-dev"
      , "manpages-fr"
      , "manpages-fr-dev"
      , "mariadb-server"
      , "menhir"
      , "ml-yacc"
      , "ocaml"
      , "opam"
      , "phpmyadmin"
      , "python3-pip"
      , "python-dev-is-python3"
      , "python-is-python3"
      , "pyzo"
      , "ragel"
      , "sqlite3"
      , "subversion"
      , "valgrind"
      , "vim"
      , "wget"
      , "zeal"
      , "zerofree"
      , "zlib1g-dev"
      ]

let snapPackages
    : List Text
    = [ "firefox" ]

let snapPackagesClassic
    : List Text
    = [ "codium", "pycharm-community" ]

let pythonPackages
    : List Text
    = [ "wheel"
      , "numpy>=1.20"
      , "scipy"
      , "scikit-learn>=1"
      , "matplotlib"
      , "pandas>=1"
      , "pillow>=8.3.2"
      , "imageio"
      , "jupyterlab==3.3.4"
      , "mypy"
      ]

let opamPackages
    : List Text
    = [ "jupyter", "utop", "ocaml-lsp-server" ]

let codiumPackages
    : List Text
    = [ "ms-ceintl.vscode-language-pack-fr"
      , "ocamllabs.ocaml-platform"
      , "ms-python.python"
      , "cpptools-linux.vsix"
      ]

let i18n =
      { mirrorCountry = "FR"
      , defaultLocale = "fr_FR.UTF-8"
      , language = "fr"
      , generatedLocales =
        [ "C.UTF-8", "fr_FR ISO-8859-1", "fr_FR.UTF-8 UTF-8" ]
      , keyboard =
        { layout = "French"
        , layoutcode = "fr"
        , modelcode = "pc105"
        , variant = "French"
        }
      }

let user =
      { username = "candidat"
      , password = "concours"
      , mysql_username = "candidat"
      , mysql_password = "concours"
      }

let hostname = "pronaos"

let virtualboxInfo =
      { ovfId = "Pronaos"
      , productUrl = "https://pronaos.sciences.re/"
      , productAnnotation = "Un système d'exploitation sympa"
      , ramSize = "4096"
      , cpuCount = "4"
      }

in  { basePackages
    , purgePackages
    , pronaosPackages
    , snapPackages
    , snapPackagesClassic
    , pythonPackages
    , opamPackages
    , codiumPackages
    , i18n
    , user
    , hostname
    , virtualboxInfo
    }
